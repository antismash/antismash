#!/bin/sh

## usage:
##      ./predictnrps.sh <adomains.faa> <dir>

## Run
perl $2/bin/allpred.pl $1
perl $2/bin/rescore.pl pid.res.tsv ind.res.tsv ens.res.tsv > sandpuma.tsv
